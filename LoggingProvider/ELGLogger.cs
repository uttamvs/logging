﻿using Logging.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Logging.LoggingProvider {
    public class ELGLogger : IELGLogger
    {
        private readonly IConfiguration _config;
        private SqlHelper _helper;
    
        public ELGLogger (IConfiguration config) 
        {
            _helper = new SqlHelper (config.GetConnectionString("ELGDatabase"));
        }
        public void LogInformation (ELGLog elgLog) {
            _helper.InsertLog(elgLog);
        }

        public void LogWarning (ELGLog elgLog) {
            _helper.InsertLog(elgLog);
        }

        public void LogError (ELGLog elgLog) {
            _helper.InsertLog(elgLog);
        }

        public void LogCritical (ELGLog elgLog) {
            _helper.InsertLog(elgLog);
        }
    }
}