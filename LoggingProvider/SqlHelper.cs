﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Logging.Models;

namespace Logging.LoggingProvider
{
    public class SqlHelper
    {
        private string ConnectionString { get; set; }

        public SqlHelper(string connectionStr)
        {
            ConnectionString = connectionStr;
        }

        private bool ExecuteNonQuery(string commandStr, List<SQLiteParameter> paramList)
        {
            bool result = false;
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SQLiteCommand command = new SQLiteCommand(commandStr, conn))
                {
                    command.Parameters.AddRange(paramList.ToArray());
                    int count = command.ExecuteNonQuery();
                    result = count > 0;
                }
            }
            return result;
        }

        public bool InsertLog(ELGLog log)
        {
            string command = $@"INSERT INTO ELG_Error_Log 
            (Event_Id,Contact_Id,Request_Id,Error_Module,Error_Location,Error_Code,Error_Description,Error_DateTime,Error_Level,ELG_LOB,ELG_SessionKey,ELG_End_User_Id) 
            VALUES (@EventID, @ContactID, @RequestID, @ErrorModule,@ErrorLocation,@ErrorCode,@ErrorDescription,@ErrorDateTime, @ErrorLevel, @ELG_LOB, @ELGSessionKey, @ELG_EndUserID)";
            List<SQLiteParameter> paramList = new List<SQLiteParameter>();
            paramList.Add(new SQLiteParameter("EventID", log.Event_Id));
            paramList.Add(new SQLiteParameter("ContactID", log.Contact_Id));
            paramList.Add(new SQLiteParameter("RequestID", log.Request_Id));
            paramList.Add(new SQLiteParameter("ErrorModule", log.Error_Module));
            paramList.Add(new SQLiteParameter("ErrorLocation", log.Error_Location));
            paramList.Add(new SQLiteParameter("ErrorCode", log.Error_Code));
            paramList.Add(new SQLiteParameter("ErrorDescription", log.Error_Description));
            paramList.Add(new SQLiteParameter("ErrorDateTime", log.Error_DateTime));
            paramList.Add(new SQLiteParameter("ErrorLevel", log.Error_Level));
            paramList.Add(new SQLiteParameter("ELG_LOB", log.ELG_LOB));
            paramList.Add(new SQLiteParameter("ELGSessionKey", log.ELG_SessionKey));
            paramList.Add(new SQLiteParameter("ELG_EndUserID", log.ELG_End_User_Id));
            return ExecuteNonQuery(command, paramList);
        }
    }
}
