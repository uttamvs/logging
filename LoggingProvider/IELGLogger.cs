﻿using Logging.Models;
using Microsoft.Extensions.Logging;

namespace Logging.LoggingProvider {
    public interface IELGLogger {
        void LogInformation (ELGLog elgLog = null);
        void LogWarning (ELGLog elgLog = null);
        void LogError (ELGLog elgLog = null);
        void LogCritical (ELGLog elgLog = null);
    }
}