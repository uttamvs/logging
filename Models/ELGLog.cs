using System;
using Newtonsoft.Json;

namespace Logging.Models {
    public class ELGLog { 
        
        [JsonIgnore]
        public int ELG_Error_Log_Id { get; set; }
        public int Event_Id { get; set; }
        public int Contact_Id { get; set; }
        public int Request_Id { get; set; }
        public string Error_Module { get; set; }
        public string Error_Location { get; set; }
        public string Error_Code { get; set; }
        public string Error_Description { get; set; }
        public string Error_DateTime { get; set; }
        public string Error_Level { get; set; }
        public string ELG_LOB { get; set; }
        public string ELG_SessionKey { get; set; }
        public int ELG_End_User_Id { get; set; }
    }
}
