﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Logging.LoggingProvider;
using Logging.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Logging.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase {

        private readonly IELGLogger _elgLogger;
        public ValuesController (IELGLogger elgLogger) {
            _elgLogger = elgLogger;
        }

        // POST api/loginformation
        [HttpPost("/LogInformation")]
        public IActionResult LogInformation (ELGLog elgLog) 
        {
            _elgLogger.LogInformation(elgLog);
            return Ok ();
        }

         // POST api/loginformation
        [HttpPost("/LogWarning")]
        public IActionResult LogWarning (ELGLog elgLog) 
        {
            _elgLogger.LogWarning(elgLog);
            return Ok ();
        }

        // POST api/LogError
        [HttpPost("/LogError")]
        public IActionResult LogError (ELGLog elgLog) 
        {
            _elgLogger.LogError(elgLog);
            return Ok ();
        }

        // POST api/LogCritical
        [HttpPost("/LogCritical")]
        public IActionResult LogCritical (ELGLog elgLog) 
        {
            _elgLogger.LogCritical(elgLog);
            return Ok ();
        }

    }
}